function multiplier(input){
    if (input === 0 ) {
        return 1;
    } else {
        return Math.pow(10, Math.floor(Math.log10(input)) + 1);
    }
}

function add(input, result = 0) {
    if (input == 0) {
        return result;
    } else {
        const newDigit = ((input%10) + 1) * multiplier(result);
        const newInput = Math.floor(input / 10);
        return add(newInput, result + newDigit);
    }
}

console.log(add(998));

/* 
How it works:

It's a recursive solution, which means we need an exit condition, and this time this is input === 0. In that case, we can simply return the result we accumulated so far.

In the other case, we have to determine what the last digit was (input % 10), add one to it, and then multiply it by a power of 10, which we calculate within the multiplier() function.

The way multiplier() works is by using the log10() function to calculate what the nearest power of 10 is. We use floor(...) + 1 to get the next power of 10, because that's what we have to multiply the new digit with.

This doesn't work for 0 though, since log10(0) doesn't exist (in JavaScript it returns -Infinity). That's why we added a separate condition if input === 0.

After that, we can just add the old result with the new digit multiplied by its multiplier, and pass that back to the add() function.
*/