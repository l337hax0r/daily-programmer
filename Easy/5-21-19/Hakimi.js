//eliminate all zeros 
const warmup1 = answers => answers.filter(answer => answer != 0);
//descending sort
const warmup2 = answers => answers.sort((a, b) => b - a);
//length check
const warmup3 = (n, answers) => n > answers.length;
//front elimination
const warmup4 = (n, answers) => answers.map((answer, idx) => idx < n ? answer - 1 : answer);

//Havel-Hakimi Algorithm
function hh(answers) {
    const sequence = warmup1(answers);
    if (sequence.length === 0) return true;
    const [first, ...rest] = warmup2(sequence);
    if (warmup3(first, rest)) return false;
    return hh(warmup4(first, rest));
}


let ans = warmup1([5, 3, 0, 2, 6, 2, 0, 7, 2, 5])
console.log(ans)