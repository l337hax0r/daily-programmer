let entry = [
    '5d12',
    '6d4',
    '1d2',
    '1d8',
    '3d6',
    '4d20',
    '100d100'
];

function parseAndPrint(qds){
    let qdsArray = qds.split('d');
    let rolls = parseInt(qdsArray[0]);
    let diceSides = parseInt(qdsArray[1]);
    let sum = 0;
    let eachRoll = '';
    for (let i = 0; i < rolls; i++){
        let x = Math.floor(Math.random() * diceSides) + 1;
        sum = sum + x;
        eachRoll = eachRoll + '[' + x + ']';
        
    };
    console.log(sum + ': ' + eachRoll);
};

for (let i = 0; i < entry.length; i++){
    parseAndPrint(entry[i]);
};