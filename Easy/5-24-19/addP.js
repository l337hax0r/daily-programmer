function additivePeristence(input) {
    if (input < 10) {
        return 0;
    }
    let acc = 0; 
    let working = input;
    while (working > 9) {
        acc = acc + working % 10;
        working = Math.floor(working / 10);
        }
    acc = acc + working;
    return (1 + additivePeristence(acc));
}

console.log(additivePeristence(13))
console.log(additivePeristence(1234))
console.log(additivePeristence(9876))
console.log(additivePeristence(199))