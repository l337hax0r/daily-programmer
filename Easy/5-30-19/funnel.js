function funnel (strOne, strTwo) {
    strOne = strOne.toString();
    strTwo = strTwo.toString();

    for (var i = 0; i < strOne.length; i++) {
        let tempStr = parseString(i, strOne);
        if (tempStr === strTwo){
            return true;
        }
    }
    return false;
}

function parseString(index, string){
    return string.substring(0, index-1) + string.substring(index);
}

console.log(funnel("leave", "eave"))
console.log(funnel("reset", "rest"))
console.log(funnel("dragoon", "dragon"))
console.log(funnel("eave", "leave"))
console.log(funnel("sleet", "lets"))
console.log(funnel("skiff", "ski"))