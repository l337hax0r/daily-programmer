const convert = (r, g, b) => {
    var hexCode = '#';
    var colorValues = [r, g, b];
    for (var i = 0; i < colorValues.length; i++) {
        hexString = colorValues[i].toString(16);
        hexCode += (hexString.length % 2 ? "0" + hexString : hexString);
    }
    console.log(`(${r}, ${g}, ${b}) => ${hexCode}`);
}

convert(255, 99, 71);
convert(184, 134, 11);
convert(189, 183, 107);
convert(0, 0, 205);