build a progress bar:

Create index.html file

Create div class "bar-container"

Create style tag base style of div container
	we consider this to be the bars empty state

create div class bar within our container
	reference it in style tag

make a button tag and onclick attribute with our function that we create in javascript

use script tag and src attribute to reference our function

within our javascript we use a query selector to select our inner bar that will be animated
	by dot syntax, select the class .bar

now that we have our bar, we need to update our style by a certain interval
	we create an interval variable
	I want my arrow function to run at every X ms

We need current percent value
	intislize a count assign it to atPercent variable
	grab our bar.style.width and assign this to our atPercent
	count up by 1

make sure the bar does not go greater than 100 percent
	create if condition
	if the atPercent is greater than or equal to 100
		we stop the interval by using clearInterval function