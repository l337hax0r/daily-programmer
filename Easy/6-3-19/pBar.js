function fillBar(seconds) {
    
    const bar = document.querySelector(".bar");
    let atPercent = 0;
    const interval = setInterval(() => {
        bar.style.width = atPercent + '%';
        atPercent++;
        console.log('Button has been clicked');
        if (atPercent >= 100) {
            clearInterval(interval);
        }

    }, (seconds * 1000) / 100 );
}